﻿using HtmlAgilityPack;
using IntevalueWebScrape.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntevalueWebScrape
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            clearFields();
            dataScrape(txtEmail.Text.Trim(), txtPassword.Text.Trim(), chkJsonIndented.Checked);
        }

        /// <summary>
        /// Starting method of data scraping
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        private async void dataScrape(string email, string password, bool isIndented)
        {
            AccountInfo account = null;
            setStatus("Processing...Please wait", true);

            // Download the Chromium revision if it does not already exist
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);

            Browser browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });

      
            Page page = await browser.NewPageAsync();
            await page.GoToAsync("https://github.com/login");

            await page.WaitForSelectorAsync("#login_field.form-control.input-block");
            await page.FocusAsync("#login_field.form-control.input-block");
            await page.Keyboard.TypeAsync(email);
            await page.WaitForSelectorAsync("#password.form-control.input-block");
            await page.FocusAsync("#password.form-control.input-block");
            await page.Keyboard.TypeAsync(password);
            await page.ClickAsync(".btn.btn-primary.btn-block");
            await page.GoToAsync("https://github.com/settings/security-log");
            
            // Store the HTML of the current page
            string content = await page.GetContentAsync();

            account = readContent(content);

            if (account != null)
            {
                rtJson.Text = serializeObject<AccountInfo>(readContent(content), isIndented);
                setStatus("Data scraped successfully", true);
            }
            else
            {
                setStatus("No record found", true);
            }
            // Close the browser
            await browser.CloseAsync();
            await Task.Delay(4000);
            setStatus(string.Empty, false);
            
        } //dataScrape method

        /// <summary>
        /// Read the content of the current page
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private AccountInfo readContent(string html)
        {
            AccountInfo account = new AccountInfo();
            Log log = null;
            List<Log> logs = new List<Log>();

            //hold the entire content of of the html page
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);

            //get only the nodes for event and time
            HtmlNodeCollection nodes = htmlDoc.DocumentNode.SelectNodes("//div[contains(@class, 'TableObject-item TableObject-item--primary')]");

            if(nodes == null)
            {
                return null;
            }

            // loop thru each node and extract only the event and time
            foreach (var item in nodes)
            {
                log = new Log();
                //get the event
                log.Event = getEventName(item.InnerHtml);
                //get the time
                log.Time = getEventTime(item.InnerHtml);
                logs.Add(log);
            }

            //get the account name
            account.Account = getAccountName(html);
            account.Logs = logs;

            htmlDoc = null;
            return account;

        }// readContent()

        /// <summary>
        /// Get event
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private string getEventName(string html)
        {
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);

            HtmlNode node = htmlDoc.DocumentNode.SelectSingleNode("//span[contains(@class, 'audit-type')]");
            return (node == null ? string.Empty : node.InnerText.Trim());
        } //getEventName(string html)

        /// <summary>
        /// Get the time of event
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private string getEventTime(string html)
        {
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);

            HtmlNode node = htmlDoc.DocumentNode.SelectSingleNode("//relative-time[contains(@class, 'no-wrap')]");

            return (node == null ? string.Empty : node.InnerText.Trim());
        } //getEventTime(string html)

        /// <summary>
        /// Get account name
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private string getAccountName(string html)
        {
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);

            HtmlNode node = htmlDoc.DocumentNode.SelectSingleNode("//a[contains(@class, 'color-text-primary')]");

            return (node == null ? string.Empty : node.InnerText.Trim());
        } //getAccountName

        /// <summary>
        /// Serialize object to Json
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string serializeObject<T>(T obj, bool isIndented)
        {
            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSettings.Formatting = (isIndented == true ? Formatting.Indented : Formatting.None);
            var jsonString = JsonConvert.SerializeObject(obj, serializerSettings);

            return jsonString;
        }

        /// <summary>
        /// Deserialize into specific object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        private T deserializeObject<T>(string json)
        {
            var obj = JsonConvert.DeserializeObject<T>(json);
            return obj;
        }

        /// <summary>
        /// Display the status of data scraping
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="isVisible"></param>
        private void setStatus(string msg, bool isVisible)
        {
            lblStatus.Visible = isVisible;
            lblStatus.Text = msg;
        }

        /// <summary>
        /// Clear fields
        /// </summary>
        private void clearFields()
        {
            rtJson.Text = string.Empty;
        }

        private void chkJsonIndented_CheckedChanged(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(rtJson.Text.Trim()))
            {
                var accountInfo = deserializeObject<AccountInfo>(rtJson.Text.Trim());
                if (accountInfo != null)
                {
                    rtJson.Text = serializeObject<AccountInfo>(accountInfo, (sender as CheckBox).Checked);
                }
            }
        }
    }
}
