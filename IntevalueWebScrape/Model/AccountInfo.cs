﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntevalueWebScrape.Model
{
    public class AccountInfo
    {
        public string Account { get; set; }

        public List<Log> Logs { get; set; }
    }
}
